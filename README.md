# Dwitter

Sources pour le TP Java + Cloud de [l'ENSSAT](http://www.enssat.fr/)

Objectifs du TP :

* Prende en main les bases du dev Java Web
* Mettre en place une architecture MVC
* Deployer sur le PAAS de Google

A faire avant le TP :

* Installer Eclipse version JEE: [http://www.eclipse.org/downloads/](http://www.eclipse.org/downloads/)
* Installer le plug-in GAE : [https://developers.google.com/eclipse/docs/download?hl=fr](https://developers.google.com/eclipse/docs/download?hl=fr)

Doc Ninja framework :
[http://www.ninjaframework.org/index.html](http://www.ninjaframework.org/index.html)

Doc Objectify :
[https://code.google.com/p/objectify-appengine/](https://code.google.com/p/objectify-appengine/)

Pour maven :

* en dev : mvn clean appengine:devserver -Pdevserver
* en prod : mvn clean appengine:update -Pupdate