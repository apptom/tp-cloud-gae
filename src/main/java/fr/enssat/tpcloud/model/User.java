package fr.enssat.tpcloud.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
@Index
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Key.
     */
    @Id
    private Long idUser;

    /**
     * User name.
     */
    @Unindex
    private String name;

    /**
     * User email.
     */
    @Index
    private String email;

    
    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
