package fr.enssat.tpcloud.model;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Post entity.
 * 
 */
@Entity
@Index
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Key.
     */
    @Id
    private Long idPost;

    /**
     *Post content.
     */
    @Unindex
    private String content;

    /**
     * Creation date.
     */
    @Index
    private Date creationDate;

    /**
     * Owner.
     */
    @Index
    private String userEmail;

    public Long getIdPost() {
        return idPost;
    }

    public void setIdPost(Long idPost) {
        this.idPost = idPost;
    }

    public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

}
