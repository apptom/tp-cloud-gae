package fr.enssat.tpcloud.dao;

import java.util.ArrayList;
import java.util.Collection;

import fr.enssat.tpcloud.model.Message;

import com.googlecode.objectify.Objectify;

import conf.OfyService;


public class MessageDao {
	
    public Collection<Message> getAllMessage() {
        Objectify ofy = OfyService.ofy();
        Collection<Message> result = ofy.load().type(Message.class).order("-creationDate").list();
        if(result == null) {
        	result = new ArrayList<Message>();
        }
        
        System.out.println("result : "+result);
        System.out.println("result size : "+result.size());
        
        return result;
    }

    /**
     * Save a new post.
     * 
     * @param post post to save
     */
    public void save(Message post) {
        Objectify ofy = OfyService.ofy();
        ofy.save().entity(post).now();
    }

    /**
     * Delete a post.
     * 
     * @param post post to delete
     */
    public void delete(Message post) {
        Objectify ofy = OfyService.ofy();
        ofy.delete().type(Message.class).id(post.getIdPost());
    }

}
