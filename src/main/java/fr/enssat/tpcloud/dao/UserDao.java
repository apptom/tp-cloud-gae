package fr.enssat.tpcloud.dao;

import java.util.Collection;

import com.googlecode.objectify.Objectify;

import fr.enssat.tpcloud.model.User;
import conf.OfyService;


public class UserDao  {


    /**
     * Get all user.
     * 
     * @return collection of user
     */
    public Collection<User> getAllUser() {
        Objectify ofy = OfyService.ofy();
        return ofy.load().type(User.class).order("-name").list();
    
    }

    /**
     * Save a new User.
     * 
     * @param user user to save
     */
    public void save(User user) {
        Objectify ofy = OfyService.ofy();
        ofy.save().entity(user);
    }

    /**
     * Delete a User.
     * 
     * @param user user to delete
     */
    public void delete(User user) {
        Objectify ofy = OfyService.ofy();
        ofy.delete().type(User.class).id(user.getIdUser());
    }

    /**
     * Find a user with this email.
     * 
     * @param email user email
     * @return user with this email or null
     */
    public User findByEmail(String email) {
        Objectify ofy = OfyService.ofy();
        User theUser = ofy.load().type(User.class).filter("email", email).first().now();
        return theUser;        
    }

}
