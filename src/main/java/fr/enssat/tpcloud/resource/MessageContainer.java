package fr.enssat.tpcloud.resource;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Post Value Object container.
 * 
 */
@XmlRootElement
public class MessageContainer {

    private List<MessageData> allMessages;

	public List<MessageData> getAllMessages() {
		return allMessages;
	}

	public void setAllMessages(List<MessageData> allMessages) {
		this.allMessages = allMessages;
	}

}
