package fr.enssat.tpcloud.resource;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Post Value Object.
 * 
 */
@XmlRootElement
public class MessageData {
    
    /**
     * Post owner real name.
     */
    private String realName;
    
    /**
     * Post owner email.
     */
    private String email;
    
    /**
     * Post content.
     */
    private String content;
    
    /**
     * Post age.
     */
    private String since;
    
    
    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getSince() {
        return since;
    }
    public void setSince(String since) {
        this.since = since;
    }
    

    
}
