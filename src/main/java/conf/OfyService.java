package conf;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import fr.enssat.tpcloud.model.Message;
import fr.enssat.tpcloud.model.User;

/**
 * Best practise for Objectify to register your entities.
 * 
 * @author ra
 * 
 */
public class OfyService {
    
    static {

        ObjectifyService.register(Message.class);
        ObjectifyService.register(User.class);

        setup();
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

    public static void setup() {

        Objectify ofy = ofy();
        User user = ofy.load().type(User.class).first().now();

        if (user == null) {

            // Create a new user and save it
            User bob = new User();
            bob.setEmail("bob@mail.com");
            bob.setIdUser(1l);
            bob.setName("bob");
            ofy.save().entity(bob).now();
        }

    }
}
