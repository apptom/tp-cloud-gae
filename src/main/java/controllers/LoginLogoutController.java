package controllers;

import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.appengine.AppEngineFilter;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.enssat.tpcloud.dao.UserDao;

/**
 * 
 * Controlleur de la gestion du login
 * 
 * @author samuel liard
 *
 */
@Singleton
@FilterWith(AppEngineFilter.class)
public class LoginLogoutController {
    
    @Inject
    UserDao userDao;
    
    /**
     * Utilisation du system de log de Google
     * 
     * @param context contexte HTTP et donc de la session
     * @return
     */
    public Result login(Context context) {

		UserService userService = UserServiceFactory.getUserService();
		boolean isLog = userService.isUserLoggedIn();

		if (isLog) {
			com.google.appengine.api.users.User currentUser = userService.getCurrentUser();
			// Mise en session de l'email pour utilisation du SecureFilter
			context.getSessionCookie().put("username", currentUser.getEmail());
            return Results.redirect("/");
		} else {
            context.getSessionCookie().remove("username");
            return Results.redirect(userService.createLoginURL("/login"));
		}

    }

    /**
     * Logout de l'utilisateur
     * 
     * @param context
     * @return
     */
    public Result logout(Context context) {
		UserService userService = UserServiceFactory.getUserService();
        context.getSessionCookie().clear();
        context.getFlashCookie().success("login.logoutSuccessful");
        return Results.redirect(userService.createLogoutURL("/"));
    }

}