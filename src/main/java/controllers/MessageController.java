package controllers;

import java.util.Date;
import java.util.Map;

import ninja.FilterWith;
import ninja.appengine.AppEngineFilter;
import ninja.params.Param;
import ninja.Results;
import ninja.Result;
import ninja.Context;
import ninja.SecureFilter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.common.collect.Maps;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import fr.enssat.tpcloud.dao.MessageDao;
import fr.enssat.tpcloud.dao.UserDao;
import fr.enssat.tpcloud.model.Message;

/**
 * Debut de gestion des messages
 * 
 * @author samuel liard
 *
 */
@Singleton
@FilterWith(AppEngineFilter.class)
public class MessageController {

    @Inject
    UserDao userDao;
    
    @Inject
    MessageDao messageDao;
	
    /**
     * Page d'accueil
     * 
     * @param context contexte HTTP
     * @return
     */
	public Result index(Context context) {

		Map<String, Object> map = Maps.newHashMap();
		map.put("allMessage", messageDao.getAllMessage());
		return Results.html().render(map);
	}

    /**
     * Ajout d'un Message.
     * 
     * @param message Le nouveau message.
     * @return 201 for success, 500 for error.
     */
    @FilterWith(SecureFilter.class)
    public Result createMessage(@Param("message") String message) {
    	
    	UserService userService = UserServiceFactory.getUserService();

    	if(!userService.isUserLoggedIn()) {
            return Results.badRequest().status(500);
    	}

    	com.google.appengine.api.users.User currentUser = userService.getCurrentUser();
        try {
            Message newMessage = new Message();
            newMessage.setContent(message);
            newMessage.setUserEmail(currentUser.getEmail());
            newMessage.setCreationDate(new Date());
            messageDao.save(newMessage);
        } catch (Exception e) {
            return Results.badRequest().status(500);
        }
        return Results.redirect("/");
    }

}
